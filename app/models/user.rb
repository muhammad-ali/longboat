class User < ActiveRecord::Base

  def authenticate(password)
    self.password.eql?(password)
  end

  def locked?
    self.login_failure_count >= 3
  end

  def reset_lock
    self.update_attributes(login_failure_count: 0)
  end

  def increment_lock
    increment = self.login_failure_count + 1
    self.update_attributes(login_failure_count: increment)
  end
end
