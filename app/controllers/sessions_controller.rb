class SessionsController < ApplicationController

  # skip_before_action :authorized, only: [:new, :create, :welcome]

  def new
  end

  def create
     @user = User.find_by(username: params[:username])
     if @user && !@user.locked?
       if @user.authenticate(params[:password])
         @user.reset_lock
         session[:user_id] = @user.id
       else
         @user.increment_lock
       end
     end
     redirect_to root_path
  end

  def logout
    session.delete(:user_id)
    redirect_to root_path
  end

  def login
  end

  def welcome
  end
end
