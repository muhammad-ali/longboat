require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'Check user lock' do
    @user = users(:one)
    assert_equal @user.locked?, false
    @user1 = users(:two)
    assert_equal @user1.locked?, true
  end

  test 'Increment failed attempts and lock user' do
    @user = users(:one)
    assert_equal @user.locked?, false
    @user.increment_lock
    @user.increment_lock
    assert_equal @user.locked?, true
  end

  test 'Reset lock' do
    @user = users(:two)
    assert_equal @user.locked?, true
    @user.reset_lock
    assert_equal @user.locked?, false
    assert_equal @user.login_failure_count, 0
  end

end
