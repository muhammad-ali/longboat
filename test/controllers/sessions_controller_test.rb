require 'test_helper'

class SessionsControllerTest < ActionController::TestCase
  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get create" do
    post :create
    assert_response :redirect

  end

  test "should Login" do
    @user = User.create(username: "abcd", password: "abc123")
    post :create, id: @user.id, username: "abcd", password: "abc123"
    assert_response :redirect
    assert_equal session[:user_id], @user.id
    u = User.find_by(id: @user.id)
    assert_equal u.locked?, false
    assert_equal u.login_failure_count, 0
  end

  test "Failed attempt" do
    @user = User.create(username: "abcd", password: "abc123")
    post :create, id: @user.id, username: "abcd", password: "abc1234"
    assert_response :redirect
    assert_not_equal session[:user_id], @user.id
    u = User.find_by(id: @user.id)
    assert_equal u.locked?, false
    assert_equal u.login_failure_count, 1
  end

  test "should get login" do
    get :login
    assert_response :success
  end

  test "should get welcome" do
    get :welcome
    assert_response :success
  end
end
